<?xml version="1.0" encoding="US-ASCII"?>
<?xml-stylesheet type='text/xsl' href='http://xml2rfc.tools.ietf.org/authoring/rfc2629.xslt' ?>
<!DOCTYPE rfc PUBLIC "-//IETF//DTD RFC 2629//EN"
"http://xml2rfc.tools.ietf.org/authoring/rfc2629.dtd">
<!--
  NOTE:  This XML file is input used to produce the authoritative copy of an
  OpenID Foundation specification.  The authoritative copy is the HTML output.
  This XML source file is not authoritative.  The statement ipr="none" is
  present only to satisfy the document compilation tool and is not indicative
  of the IPR status of this specification.  The IPR for this specification is
  described in the "Notices" section.  This is a public OpenID Foundation
  document and not a private document, as the private="..." declaration could
  be taken to indicate.
-->
<rfc category="std" docName="openid-heart-fhir-oauth2" ipr="none">
  <?rfc toc="yes" ?>

  <?rfc tocdepth="5" ?>

  <?rfc symrefs="yes" ?>

  <?rfc sortrefs="yes"?>

  <?rfc strict="yes" ?>

  <?rfc iprnotified="no" ?>

  <?rfc private="Final" ?>

  <front>
    <title abbrev="HEART FHIR OAuth 2.0">Health Relationship Trust Profile for
    Fast Healthcare Interoperability Resources (FHIR) OAuth 2.0 Scopes</title>

    <author fullname="Justin Richer" initials="J." role="editor"
            surname="Richer">
      <address>
        <email>openid@justin.richer.org</email>

        <uri/>
      </address>
    </author>

    <author fullname="Josh Mandel" initials="J." surname="Mandel">
      <organization>Harvard Medical School Department of Biomedical
      Informatics</organization>

      <address>
        <postal>
          <street/>

          <city/>

          <region/>

          <code/>

          <country/>
        </postal>

        <phone/>

        <facsimile/>

        <email>Joshua.Mandel@childrens.harvard.edu</email>

        <uri/>
      </address>
    </author>

    <date day="25" month="September" year="2015"/>

    <workgroup>OpenID Heart Working Group</workgroup>

    <abstract>
      <t>FHIR is an HTTP-based, resource-oriented RESTful API based on a set
      of clinical, administrative, financial, and infrastructure resource
      definitions. The API supports create, read, update, delete, and search
      operations, as well as a framework for ad-hoc operations.</t>

      <t>The OAuth 2.0 protocol framework defines a mechanism to allow a
      resource owner to delegate access to a protected resource for a client
      application, optionally limited by a set of scopes.</t>

      <t>This specification profiles the OAuth 2.0 protocol scopes to be used
      with the FHIR protocol to increase baseline security, provide greater
      interoperability, and structure deployments in a manner specifically
      applicable to (but not limited to) the healthcare domain.</t>
    </abstract>
  </front>

  <middle>
    <section anchor="Introduction" title="Introduction">
      <t>This document profiles the OAuth 2.0 web authorization framework for
      use in the context of securing Representational State Transfer (RESTful)
      interfaces using the Fast Health Interoperable Resources (FHIR)
      protocol. The FHIR OAuth 2.0 scope specifications accommodate a wide
      range of implementations with varying security and usability
      considerations, across different types of software clients. To achieve
      this flexibility, the standard makes many security controls optional.
      OAuth implementations using only the minimum mandatory security measures
      require minimal effort on the part of developers and users, but they
      also fail to prevent known attacks and are unsuitable for protecting
      sensitive data. The FHIR OAuth 2.0 profile defined in this document
      serve to define a baseline set of FHIR OAuth scopes suitable for a wide
      range of use cases, while maintaining reasonable ease of implementation
      and functionality.</t>

      <section anchor="rnc" title="Requirements Notation and Conventions">
        <t>The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
        "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and
        "OPTIONAL" in this document are to be interpreted as described in
        <xref target="RFC2119">RFC 2119</xref>.</t>

        <t>All uses of <xref target="RFC7517">JSON Web Signature (JWS)</xref>
        and <xref target="RFC7518">JSON Web Encryption (JWE)</xref> data
        structures in this specification utilize the JWS Compact Serialization
        or the JWE Compact Serialization; the JWS JSON Serialization and the
        JWE JSON Serialization are not used.</t>
      </section>

      <section anchor="Terminology" title="Terminology">
        <t>This specification uses the terms "Access Token", "Authorization
        Code", "Authorization Endpoint", "Authorization Grant", "Authorization
        Server", "Client", "Client Authentication", "Client Identifier",
        "Client Secret", "Grant Type", "Protected Resource", "Redirection
        URI", "Refresh Token", "Resource Owner", "Resource Server", "Response
        Type", and "Token Endpoint" defined by <xref target="RFC6749">OAuth
        2.0</xref>, the terms "Claim Name", "Claim Value", and "JSON Web Token
        (JWT)" defined by <xref target="RFC7519">JSON Web Token (JWT)</xref>,
        and the terms defined by <xref target="OpenID.Core">OpenID Connect
        Core 1.0</xref>.</t>
      </section>
    </section>

    <section anchor="Scopes" title="Scopes design">
      <t>In OAuth, scopes define individual pieces of authority that can be
      requested by clients, granted by resource owners, and enforced by
      resource servers. Specific scope values will be highly dependent on the
      specific types of resources being protected in a given interface. <xref
      target="OpenID.Core">OpenID Connect</xref>, for example, defines scope
      values to enable access to different attributes of user profiles.</t>

      <t>Scopes can be used to indicate access to different kinds of things in
      an API. Common examples include:</t>

      <t><list style="symbols">
          <t>The kind of resource being protected (medications,
          appointments).</t>

          <t>The kind of access to the resource being requested (read, write,
          delete).</t>

          <t>The specific resource being accessed (user ID, resource URI).</t>
        </list></t>

      <t>While all of these are valid means for defining scopes, not all of
      them work for different kinds of APIs. </t>

      <t>In this specification, the scope values are a composite string
      describing the type of permission, the type of resource, and the type of
      access to that resource being requested. These scopes are plain strings
      made by combining the permission type, followed by a single forward
      slash "/" character, followed by the resource type, followed by a single
      period character ".", followed by the access type. The asterisk
      character "*" is reserved as a special value to stand in for any valid
      value within a category.</t>

      <t>The entire scope definition is:</t>

      <t><spanx style="verb">scope := permission/resource.access</spanx></t>

      <t><spanx style="verb">permission, resource, access := any string (without space, period, slash, or asterisk) | asterisk</spanx></t>

      <t>Strings within each category MUST NOT include the space " ", period
      ".", forward slash "/", or asterisk "*" character.</t>

      <t>Implementation of these scope strings is a matter of choice for the
      OAuth system. The authorization server and protected resource MAY decide
      to pre-compute all viable combinations within the system as simple
      strings as shown in <xref target="ComputedScopeValues"/>, or they MAY
      decide to parse the values within the strings at run time.</t>

      <t>Protected resources MUST define and document which scopes are
      required for access to the resource, listing all appropriate scope
      component combinations including wildcards.</t>

      <t>Authorization servers SHOULD define and document default scope values
      that will be used if an authorization request does not specify a
      requested set of scopes.</t>

      <section anchor="Permission" title="Permission type">
        <t>The permission type component of a scope definition indicates
        whether the access being requested is for a single patient record or a
        bulk set of patient records. This specification defines two possible
        values.</t>

        <t><list style="hanging">
            <t hangText="patient">The scope is for a single patient's record,
            either for the current user or someone else that they have been
            given access to.</t>

            <t hangText="user">The scope is for a bulk set of records or for
            aggregate data not representing a single patient, based on what is
            available to the current user.</t>
          </list></t>

        <t>Extensions to this specification MAY define additional permission
        types using the registry defined in <xref target="iana"/>. </t>

        <t>This specification does not define a wildcard (asterisk, "*")
        general resource type definition.</t>
      </section>

      <section anchor="Resource" title="Resource type">
        <t>The resource type indicates the kind of FHIR resource and
        consequently the kind of information available at it. This
        specification defines the following possible values.</t>

        <t><list style="hanging">
            <t hangText="Patient">Demographic information about the
            patient.</t>

            <t hangText="MedicationOrder">Information about orders for
            medications.</t>

            <t hangText="MedicationDispense">Information about supply of
            medications to a patient.</t>

            <t hangText="MedicationAdministration">Information about
            medications consumption or other administration.</t>

            <t hangText="MedicationStatement">Information about the believed
            state of a medication.</t>

            <t hangText="Observation">Information about observations performed
            by a healthcare provider.</t>

            <t hangText="Appointment">Information about scheduled
            appointments.</t>

            <t hangText="*">Wildcard representing all available resources
            under the given context.</t>
          </list></t>

        <t>Extensions to this specification MAY define additional specific
        resource types using the registry defined in <xref target="iana"/>.
        The name of the resource type SHOULD match the name of the associated
        FHIR resource.</t>
      </section>

      <section anchor="Access" title="Access type">
        <t>The access type defines the kinds of actions that can be taken on a
        particular resource. This specification defines the following access
        types.</t>

        <t><list style="hanging">
            <t hangText="read">Allows information to be read from the
            resource.</t>

            <t hangText="write">Allows information to be read from or written
            to the resource.</t>

            <t hangText="*">Wildcard representing all possible actions under
            the given context.</t>
          </list></t>

        <t>Extensions to this specification MAY define additional specific
        resource types using the registry defined in <xref
        target="iana"/>.</t>
      </section>
    </section>

    <section title="Resource selection">
      <t>In many OAuth transactions, the client does not need to indicate to
      the authorization server the protected resource that it is trying to
      access, as that information is discernible from the context of the
      scopes and the resource owner present during authorization. For example,
      a patient authorizing access to their own record would not need to
      specify which record is theirs since the resource server would
      presumably be able to find the record based on the identity of the
      resource owner. </t>

      <t>In other transactions, the resource owner will need to specify to the
      authorization server and resource server which protected resource is to
      be accessed. For example, a user with access to not only their own
      patient record but also that of their children would need to specify
      which record is to be accessed by the client. </t>

      <t>For interactive OAuth flows such as the authorization code and
      implicit flows, the authorization server can present a selection screen
      to the user on the authorization screen, allowing them to select from a
      set of available options. Alternatively, if the client knows the
      protected resource that it is trying to gain access to, it can indicate
      this protected resource to the authorization server during the request
      to the authorization endpoint (for the authorization code and implicit
      flow) or the token endpoint (for the client credentials flow) using the
      OPTIONAL <spanx style="verb">aud</spanx> (audience) parameter.</t>

      <t><list style="hanging">
          <t hangText="aud">A URI string representing the protected resource
          which this authorization request is directed toward. This URI MUST
          be a patient's record consisting of many resources or a specific
          FHIR resource for a given patient.</t>
        </list></t>

      <t>If the client does not provide an <spanx style="verb">aud</spanx>
      parameter in its request, the authorization server MUST perform one of
      these three actions:</t>

      <t><list style="symbols">
          <t>Return an invalid_request error response.</t>

          <t>Select a default resource based on the resource owner.</t>

          <t>Allow the resource owner to interactively select or specify an
          appropriate resource on the authorization page.</t>
        </list></t>

      <t>The authorization server SHOULD either select a default resource or
      give the resource owner an opportunity to specify the resource if none
      is selected, reserving the error response for cases in which neither of
      these are possible.</t>
    </section>

    <section anchor="iana" title="IANA Considerations">
      <t>This specification creates three registries for the values in the
      three different components of the scope.</t>
    </section>
  </middle>

  <back>
    <references title="Normative References">
      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.2119"?>

      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.2246"?>

      <?rfc include='http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.3986'?>

      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.5246"?>

      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.5322"?>

      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.5646"?>

      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.5785"?>

      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.6125"?>

      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.6749"?>

      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.6750"?>

      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.6819"?>

      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.7033"?>

      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.7517"?>

      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.7518"?>

      <?rfc include="http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.7519"?>

      <reference anchor="OpenID.Core">
        <front>
          <title>OpenID Connect Core 1.0</title>

          <author fullname="Nat Sakimura" initials="N." surname="Sakimura">
            <organization abbrev="NRI">Nomura Research Institute,
            Ltd.</organization>
          </author>

          <author fullname="John Bradley" initials="J." surname="Bradley">
            <organization abbrev="Ping Identity">Ping Identity</organization>
          </author>

          <author fullname="Michael B. Jones" initials="M.B." surname="Jones">
            <organization abbrev="Microsoft">Microsoft</organization>
          </author>

          <author fullname="Breno de Medeiros" initials="B."
                  surname="de Medeiros">
            <organization abbrev="Google">Google</organization>
          </author>

          <author fullname="Chuck Mortimore" initials="C." surname="Mortimore">
            <organization abbrev="Salesforce">Salesforce</organization>
          </author>

          <date day="8" month="November" year="2014"/>
        </front>

        <format target="http://openid.net/specs/openid-connect-core-1_0.html"
                type="HTML"/>
      </reference>
    </references>

    <section anchor="Acknowledgements" title="Acknowledgements">
      <t>The OpenID Community would like to thank the following people for
      their contributions to this specification:</t>

      <t/>
    </section>

    <section anchor="ComputedScopeValues" title="Fully computed scope values">
      <t>The scopes in this specification are composed from individual
      components, but scopes in OAuth are treated as string by many parts of
      the system. Clients, authorization servers, and protected resources
      should not be expected to compose these strings, merely be able to
      understand the rights associated with a given string. Consequently, we
      are providing here a table of pre-computed values for all possible scope
      string combinations defined form the components in this document, along
      with their meaning.</t>

      <t><list style="hanging">
          <t hangText="patient/Patient.read">Read access to a single patient's
          demographic information.</t>

          <t hangText="patient/Patient.write">Read and write access to a
          single patient's demographic information.</t>

          <t hangText="patient/Patient.*">Full access to a single patient's
          demographic information.</t>

          <t hangText="patient/MedicationOrder.read">Read access to a single
          patient's orders for medications.</t>

          <t hangText="patient/MedicationOrder.write">Read and write access to
          a single patient's orders for medications.</t>

          <t hangText="patient/MedicationOrder.*">Full access to a single
          patient's orders for medications.</t>

          <t hangText="patient/MedicationDispense.read">Read access to supply
          of medications to a single patient.</t>

          <t hangText="patient/MedicationDispense.write">Read and write access
          to supply of medications to a single patient.</t>

          <t hangText="patient/MedicationDispense.*">Full access to supply of
          medications to a single patient.</t>

          <t hangText="patient/MedicationAdministration.read">Read access to a
          single patient's medications consumption or other
          administration.</t>

          <t hangText="patient/MedicationAdministration.write">Read and write
          access to a single patient's medications consumption or other
          administration.</t>

          <t hangText="patient/MedicationAdministration.*">Full access to a
          single patient's medications consumption or other
          administration.</t>

          <t hangText="patient/MedicationStatement.read">Read access to the
          believed state of a medication for a single patient.</t>

          <t hangText="patient/MedicationStatement.write">Read and write
          access to the believed state of a medication for a single
          patient.</t>

          <t hangText="patient/MedicationStatement.*">Full access to the
          believed state of a medication for a single patient.</t>

          <t hangText="patient/Observation.read">Read access to a single
          patient's observations performed by a healthcare provider.</t>

          <t hangText="patient/Observation.write">Read and write access to a
          single patient's observations performed by a healthcare
          provider.</t>

          <t hangText="patient/Observation.*">Full access to a single
          patient's observations performed by a healthcare provider.</t>

          <t hangText="patient/Appointment.read">Read access to a single
          patient's scheduled appointments.</t>

          <t hangText="patient/Appointment.write">Read and write access to a
          single patient's scheduled appointments.</t>

          <t hangText="patient/Appointment.*">Full access to a single
          patient's scheduled appointments.</t>

          <t hangText="patient/*.read">Read access to a single patient's
          complete records.</t>

          <t hangText="patient/*.write">Read and write access to a single
          patient's complete records.</t>

          <t hangText="patient/*.*">Full access to a single patient's complete
          records.</t>

          <t hangText="user/Patient.read">Read access to all authorized
          demographic information.</t>

          <t hangText="user/Patient.write">Read and write access to all
          authorized demographic information.</t>

          <t hangText="user/Patient.*">Full access to all authorized
          demographic information.</t>

          <t hangText="user/MedicationOrder.read">Read access to all
          authorized orders for medications.</t>

          <t hangText="user/MedicationOrder.write">Read and write access to
          all authorized orders for medications.</t>

          <t hangText="user/MedicationOrder.*">Full access to all authorized
          orders for medications.</t>

          <t hangText="user/MedicationDispense.read">Read access to all
          authorized supply of medications.</t>

          <t hangText="user/MedicationDispense.write">Read and write access to
          all authorized supply of medications.</t>

          <t hangText="user/MedicationDispense.*">Full access to all
          authorized supply of medications.</t>

          <t hangText="user/MedicationAdministration.read">Read access to all
          authorized medications consumption or other administration.</t>

          <t hangText="user/MedicationAdministration.write">Read and write
          access all authorized medications consumption or other
          administration.</t>

          <t hangText="user/MedicationAdministration.*">Full access to all
          authorized medications consumption or other administration.</t>

          <t hangText="user/MedicationStatement.read">Read access to all
          authorized believed states of medication.</t>

          <t hangText="user/MedicationStatement.write">Read and write access
          to all authorized believed states of medication.</t>

          <t hangText="user/MedicationStatement.*">Full access to all
          authorized believed states of medication.</t>

          <t hangText="user/Observation.read">Read access to all authorized
          observations performed by a healthcare provider.</t>

          <t hangText="user/Observation.write">Read and write access to all
          authorized observations performed by a healthcare provider.</t>

          <t hangText="user/Observation.*">Full access to all authorized
          observations performed by a healthcare provider.</t>

          <t hangText="user/Appointment.read">Read access to all authorized
          scheduled appointments.</t>

          <t hangText="user/Appointment.write">Read and write access to all
          authorized scheduled appointments.</t>

          <t hangText="user/Appointment.*">Full access to all authorized
          scheduled appointments.</t>

          <t hangText="user/*.read">Read access to all authorized complete
          records.</t>

          <t hangText="user/*.write">Read and write access to all authorized
          complete records.</t>

          <t hangText="user/*.*">Full access to all authorized complete
          records.</t>
        </list></t>
    </section>

    <section anchor="Notices" title="Notices">
      <t>Copyright (c) 2015 The OpenID Foundation.</t>

      <t>The OpenID Foundation (OIDF) grants to any Contributor, developer,
      implementer, or other interested party a non-exclusive, royalty free,
      worldwide copyright license to reproduce, prepare derivative works from,
      distribute, perform and display, this Implementers Draft or Final
      Specification solely for the purposes of (i) developing specifications,
      and (ii) implementing Implementers Drafts and Final Specifications based
      on such documents, provided that attribution be made to the OIDF as the
      source of the material, but that such attribution does not indicate an
      endorsement by the OIDF.</t>

      <t>The technology described in this specification was made available
      from contributions from various sources, including members of the OpenID
      Foundation and others. Although the OpenID Foundation has taken steps to
      help ensure that the technology is available for distribution, it takes
      no position regarding the validity or scope of any intellectual property
      or other rights that might be claimed to pertain to the implementation
      or use of the technology described in this specification or the extent
      to which any license under such rights might or might not be available;
      neither does it represent that it has made any independent effort to
      identify any such rights. The OpenID Foundation and the contributors to
      this specification make no (and hereby expressly disclaim any)
      warranties (express, implied, or otherwise), including implied
      warranties of merchantability, non-infringement, fitness for a
      particular purpose, or title, related to this specification, and the
      entire risk as to implementing this specification is assumed by the
      implementer. The OpenID Intellectual Property Rights policy requires
      contributors to offer a patent promise not to assert certain patent
      claims against other contributors and against implementers. The OpenID
      Foundation invites any interested party to bring to its attention any
      copyrights, patents, patent applications, or other proprietary rights
      that may cover technology that may be required to practice this
      specification.</t>
    </section>

    <section title="Document History">
      <t/>

      <t>-2015-09-16</t>

      <t><list style="symbols">
          <t>Created first semantic scope profile</t>
        </list></t>
    </section>
  </back>
</rfc>
